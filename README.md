This Docker image contains: 

* Ubuntu Trusty (64-bit)
* build-essential, git
* Qt5.6, QtWebEngine (from ppa:beineri/opt-qt56-trusty)
* Ruby 2.3 (from ppa:brightbox/ruby-ng)

